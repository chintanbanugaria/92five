Before proceeding to installation please make sure that your server meets the minimum server requirements:

## Minimum Server Requirements ##

* PHP 5.4 or greater
* PDO PHP extendsion
* MCrypt PHP extendsion
* GD PHP library
* MySql Database

If your webserver is running Apache then mod_rewrite should be installed

# Installation #

* Please make sure that directory / sub-directory in which you are installing the application is empty
* Download the Zip Archive from [here](http://92fiveapp.com/download)
* Unzip the archive to the prepared directory
* Grant writing permission to app/storage, assets/uploads and assets/images/profilepics
* Navigate to the /install to start the wizard and follow the instruction

# For any help / quries / errors please visit forums.92fiveapp.com #